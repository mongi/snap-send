const fs = require('fs');
const env = process.argv[2];
const config = require('./db.config');



(async function buildenv(env) {
    let exportENV = "";
    let PORT = env === "prod" ? 3000 : 8000
    let DB_NAME = env == "prod" ? "myapp" : "myapp-dev";
    //let DB_HOST_NAME = env == "prod" ? "vps630188.ovh.net" : "vps613589.ovh.net"
    let DB_HOST_NAME = "vps613589.ovh.net"
    exportENV += `export SNAP_ENV="${env}"\n`;
    exportENV += `export SNAP_PORT=${PORT}\n`;
    exportENV += `export SNAP_DB_NAME="${DB_NAME}"\n`;
    exportENV += `export SNAP_DB_ENDPOINT=${DB_HOST_NAME}\n`;
    exportENV += `export SNAP_DB_PASSWORD=${config.DB_PASSWORD}\n`;
    exportENV += `export SNAP_DB_USER_NAME=${config.DB_USER_NAME}\n`;

    await fs.writeFileSync(`./.env_${env}`, exportENV);

})(env)
