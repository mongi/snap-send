import express from 'express';
import path from 'path'
import { DashController } from './core';
import { ApiRouter } from './api';

var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser')

// Create a new express application instance
const app: express.Application = express();
let port: any|number = process.env.SNAP_PORT || 3000;


// view engine setup && public directory
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, 'public')));

//config express
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(bodyParser.json({limit: "50mb"}));
app.use(bodyParser.urlencoded({limit: "50mb", extended: true, parameterLimit:50000}));

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
})


// Mount the WelcomeController at the /welcome route
app.use('/', DashController);
app.use('/api', ApiRouter)

// Serve the application at the given port
app.listen(port, () => {
    // Success callback
    console.log(process.env.PWD)
    console.log(`Listening at http://localhost:${port}/`);
});
