
import { Router, Request, Response } from 'express';
import db from '../db/connect';
import Sequelize  from "sequelize";
import path from "path"
import fs from "fs"

var JSZip = require('jszip');
var Docxtemplater = require('docxtemplater');
var ImageModule = require('open-docxtemplater-image-module');


 

const router: Router = Router();

router.get('/', async(req: Request, res: Response) => {

    const db_con = await db();

    let projects = await db_con.query('SELECT * FROM myapp.dp_projects',{ type: Sequelize.QueryTypes.SELECT })
    .then(rows => {
      return rows
    })
    .catch(err => console.log(err))

    res.render('index', {title: "Snap && Send", projects:  projects})
});


router.post('/generate_doc', async(req, res, next) =>{

    const db_con: any = await db()

    let filenameTosave: string = `template/${(req.body.name).replace(" ","")}_${req.body.domaine}.docx`
    let pathFile: string = path.resolve(__dirname, `../public/${filenameTosave}`);
    let QUERY_EXEC: string = `SELECT id, image64, domaine FROM myapp.dp_project_data where dp_project_data.projectId=${req.body.id} and dp_project_data.domaine='${req.body.domaine}';`;
    
    await db_con.query(QUERY_EXEC,{ type: Sequelize.QueryTypes.SELECT })
    .then(async(rows) => 
        {
                if (rows.length  ==  0 ) {
                    let $res =   {
                        msg: "Pas de constat sauvegardé dans ce projet"
                    }
                    res.json($res).end()
                }

                let dataToSet = [];
                let $response = {};
                let i = 1;
  
  
                var content = await fs.readFileSync(path.resolve(__dirname, '../public/template/template.docx'), 'binary');
  
                var zip = new JSZip(content);
                var doc = new Docxtemplater();
  
                var opts:any  = {}
                opts.centered = true;
                opts.fileType= "docx"; 
  
                opts.getImage = function(tagValue, tagName) {
                    return fs.readFileSync(`${process.env.PWD}/public/uploads/${tagValue}`);
                }
                opts.getSize = function(img, tagValue, tagName) {
                    return [300, 220];
                }
                var imageModule = new ImageModule(opts);
  
                await rows.map((item) => {
                   
                    dataToSet.push({
                      'name': req.body.name,
                      'domaine': item.domaine == "equipement" ? "Équipements technique" : "Clos et Couvert" ,
                      'image': item.image64,
                      "num_constat": i
                    })
                    i++
                })
  
                await doc.attachModule(imageModule)
                await doc.loadZip(zip);
  
                doc.setData({
                  projects: dataToSet
                });
  
                try{
                    await  doc.render()
                }
                catch (error) {
                    console.log("error to generate a doc ", error)
                    throw error;
                }
  
                let buf = await doc.getZip().generate({type: 'nodebuffer'});
                if (fs.existsSync(pathFile)) {
                  await fs.unlinkSync(pathFile)
                }
                await fs.writeFile(pathFile, buf, (err) => {
                    if (err) throw err;
                    res.status(200).json({url: filenameTosave}).end();
                });
  
        }
    )
    .catch(err => res.send(`error when gen docx ${err}`).end())


} ) 

export const DashController: Router = router;
