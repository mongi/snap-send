import { Router, Request, Response } from 'express';
import db from '../db/connect';
import Sequelize  from "sequelize";
import  {imageFilter} from "../utils/utils"

const crypto = require('crypto');
const multer = require("multer");

const router: Router = Router();


/**
 * Upload Multer Config
 */
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, `public/uploads/`)
    },
    filename: function (req, file, cb) {
      crypto.pseudoRandomBytes(16, function (err, raw) {
        cb(null, raw.toString('hex') + Date.now() + '.' + file.mimetype.split('/')[1]);
      });
    }
  });
var upload = multer({ storage: storage,  fileFilter: imageFilter });


/**
 * @summary Get All projects
 * @function GetALLProjects
*/

/* GET projects listing. */
router.get('/projects', async(req: Request, res: Response) => {

    const db_con = await db();

    await db_con.query('SELECT * FROM myapp.dp_projects',{ type: Sequelize.QueryTypes.SELECT })
    .then(projects => {
        res.status(200).json(projects).end();
    })
    .catch(err => {
        res.send(404).end()
    })
});


/**
 * @function PostNewProject
 * @param name
*/
router.post('/projects', async (req, res, next) => {
    let name = req.body.name;
    const sqlAddProject = `INSERT INTO myapp.dp_projects (name) values ('${name}')`;

    const db_con = await db();

    await db_con.query(sqlAddProject,{ type: Sequelize.QueryTypes.INSERT })
    .then(() =>
        {
            res.status(200).send('New Project added successfully ').end()
        }
    )
    .catch(err =>res.send('Insertion failed ' + JSON.stringify(err)).end())
});


/**
* @function Api to send data from devices
*/


/* POST project. */
router.post('/data_projects', async (req, res, next) => {
    let projectId = req.body.projectId,
        domaine = req.body.domaine,
        image = req.body.image;

    const db_con = await db();
    const sqlAddProjectData = `INSERT INTO myapp.dp_project_data (image64, domaine, projectId) values ('${image}', '${domaine}','${projectId}')`;


    await db_con.query(sqlAddProjectData,{ type: Sequelize.QueryTypes.INSERT })
    .then(() =>
        {
            res.status(200).send('New Project  data added successfully ').end()
        }
    )
    .catch(err =>res.send('Insertion failed ' + JSON.stringify(err)).end())

});
/**
 * @function UploadSingleFile
 */
router.post('/upload', upload.single('avatar'),  (req: any, res: Response)  => {
    res.json(req.file.filename).end()
})




export const ApiRouter: Router = router;
