import Sequelize  from "sequelize";


export default async function() : Promise<any> {
    return new Promise(async(resolve, reject) => {
        try {
 
            let connection: any =  new Sequelize(
              `${process.env.SNAP_DB_NAME}`,`${process.env.SNAP_DB_USER_NAME}`, `${process.env.SNAP_DB_PASSWORD}`,
              {
                host: `${process.env.SNAP_DB_ENDPOINT}`,
                dialect: 'mysql',
                logging: true,
                pool: {
                    acquire: 5000,
                    idle: 2000
                },
                define: {
                    timestamps: false
                }
            });

            resolve(connection);

        } catch (error) {
            reject(error)
        }
    })
}
