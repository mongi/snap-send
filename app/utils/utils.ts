import fs  from "fs"

export  function imageFilter(req, file, cb) {
    // accept image only
    if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
        return cb(new Error('Only image files are allowed!'), false);
    }
    cb(null, true);
};

// function to encode file data to base64 encoded string
export async function encodeImage(file) {
    let bitmap = fs.readFileSync(`${process.env.PWD}/public/uploads/${file}`);
    let extension = file.split('.')[1]
    let imgBuffer = `data:image/${extension};base64,${new Buffer(bitmap).toString('base64')}` ;
 
    return imgBuffer
}
 
export async function getImageList(arr) {
     let $arr = {}
     arr.map(async(r) => {
         let key = 'image_' + r.id
         $arr[key] = await encodeImage(r.image64)
     })
     return $arr
 }