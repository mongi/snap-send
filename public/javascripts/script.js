
$(document).ready(function() {

 $('#submitProject').on('click', function(){
        if($('#namePrj').val() != "")
        {
            $.ajax({
                url : "/api/projects",
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify({"name": $('#namePrj').val()}),
                success: function(res){
                    window.location.reload()
                }
            })
        }
        else{
            alert('le champ nom de projet est vide')
        }

 })

    $('[data-project-id]').on('click', function() {
            let id = $(this).data('projectId'),
                name = $(this).data('projectName'),
                domaine = $(this).data('projectDomaine')
            $.ajax({
                url : "/generate_doc",
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify({"id": id, "name": name, "domaine": domaine}),
                success: function(res){
                    if(res.msg) {alert(res.msg)}
                    else {
                        window.location.href = res.url;
                     }
                    
                }
            })
    })

})